from django.utils.translation import gettext_lazy as _

LANGUAGES = (
    ## Customize this
    ('it', _('it')),
    ('en', _('en')),
    ('es', _('es')),
    ('pt', _('pt')),
    ('fr', _('fr')),
    ('de', _('de')),
    ('nl', _('nl')),
)
