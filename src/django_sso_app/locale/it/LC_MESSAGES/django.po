# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-17 13:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: django_sso_app/backend/pages/admin.py:11
msgid "Advanced options"
msgstr "Opzioni avanzate"

#: django_sso_app/backend/templates/account/account_inactive.html:5
#: django_sso_app/backend/templates/account/account_inactive.html:8
#: django_sso_app_qcumber_frontend/templates/account/account_inactive.html:5
msgid "Account Inactive"
msgstr "Account disabilitato"

#: django_sso_app/backend/templates/account/account_inactive.html:10
msgid "This account is inactive."
msgstr "Questo account non è attivo."

#: django_sso_app/backend/templates/account/email.html:6
msgid "Account"
msgstr ""

#: django_sso_app/backend/templates/account/email.html:9
msgid "E-mail Addresses"
msgstr "Indirizzo E-mail"

#: django_sso_app/backend/templates/account/email.html:12
msgid "The following e-mail addresses are associated with your account:"
msgstr "Di seguito gli indirizzi e-mail associati al tuo account:"

#: django_sso_app/backend/templates/account/email.html:26
msgid "Verified"
msgstr "Verificato"

#: django_sso_app/backend/templates/account/email.html:28
msgid "Unverified"
msgstr "Non verificato"

#: django_sso_app/backend/templates/account/email.html:30
msgid "Primary"
msgstr "Principale"

#: django_sso_app/backend/templates/account/email.html:36
msgid "Make Primary"
msgstr "Rendi primario"

#: django_sso_app/backend/templates/account/email.html:37
msgid "Re-send Verification"
msgstr "Reinvia la verifica"

#: django_sso_app/backend/templates/account/email.html:38
msgid "Remove"
msgstr "Elimina"

#: django_sso_app/backend/templates/account/email.html:45
msgid "Warning:"
msgstr "Attenzione:"

#: django_sso_app/backend/templates/account/email.html:45
msgid "You currently do not have any e-mail address set up. You should really add an e-mail address so you can receive notifications, reset your password, etc."
msgstr "Non hai ancora impostato un indirizzo e-mail. E' necessario impostarlo per ricevere notifiche, aggiornamenti, ecc.."

#: django_sso_app/backend/templates/account/email.html:50
msgid "Add E-mail Address"
msgstr "Aggiungi indirizzo e-mail"

#: django_sso_app/backend/templates/account/email.html:55
msgid "Add E-mail"
msgstr "Aggiungi E-mail"

#: django_sso_app/backend/templates/account/email.html:65
msgid "Do you really want to remove the selected e-mail address?"
msgstr "Vuoi veramente eliminare l'indirizzo e-mail selezionato?"

#: django_sso_app/backend/templates/account/email/base_email.html:184
msgid "This message has been set by:"
msgstr "Questo messaggio è stato inviato da"

#: django_sso_app/backend/templates/account/email/email_confirmation_message.html:3
#: django_sso_app/backend/templates/account/email/password_reset_key_message.html:3
#: django_sso_app_qcumber_frontend/templates/account/email/email_confirmation_message.html:3
#: django_sso_app_qcumber_frontend/templates/account/email/password_reset_key_message.html:3
#, python-format
msgid "Hello from %(EMAILS_SITE_NAME)s!"
msgstr "Un saluto da %(EMAILS_SITE_NAME)s!"

#: django_sso_app/backend/templates/account/email/email_confirmation_message.html:6
#: django_sso_app_qcumber_frontend/templates/account/email/email_confirmation_message.html:6
#, python-format
msgid ""
"You're receiving this e-mail because user %(user_display)s has given yours as an e-mail address to connect their account.\n"
"To confirm this is correct, go to "
msgstr ""
"Hai ricevuto questa e-mail poichè l'utente %(user_display)s ha specificato questo indirizzo per il suo account.\n"
"Per confermare che è corretto, vai su "

#: django_sso_app/backend/templates/account/email/email_confirmation_message.html:9
#: django_sso_app_qcumber_frontend/templates/account/email/email_confirmation_message.html:9
#, python-format
msgid "Thank you from %(EMAILS_SITE_NAME)s!"
msgstr "Un ringraziamento da %(EMAILS_SITE_NAME)s!"

#: django_sso_app/backend/templates/account/email/password_reset_key_message.html:7
#: django_sso_app_qcumber_frontend/templates/account/email/password_reset_key_message.html:7
msgid ""
"You're receiving this e-mail because you or someone else has requested a password for your user account.\n"
"It can be safely ignored if you did not request a password reset. Click the link below to reset your password."
msgstr ""
"Hai ricevuto questa e-mail perchè è stato chiesto di reimpostare la password per il tuo account.\n"
"Se lo desideri puoi semplicemente ignorarlo. Clicca sul link sottostante per procedere."

#: django_sso_app/backend/templates/account/email/password_reset_key_message.html:19
#: django_sso_app_qcumber_frontend/templates/account/email/password_reset_key_message.html:19
#, python-format
msgid "In case you forgot, your username is %(username)s."
msgstr "Nel caso l'avessi dimenticato, il tuo nome utente è %(username)s."

#: django_sso_app/backend/templates/account/email/password_reset_key_message.html:20
#: django_sso_app_qcumber_frontend/templates/account/email/password_reset_key_message.html:20
#, python-format
msgid "Thank you for using %(EMAILS_SITE_NAME)s!"
msgstr "Grazie per aver usato %(EMAILS_SITE_NAME)s!"

#: django_sso_app/backend/templates/account/email_confirm.html:6
#: django_sso_app/backend/templates/account/email_confirm.html:10
#: django_sso_app_qcumber_frontend/templates/account/email_confirm.html:5
msgid "Confirm E-mail Address"
msgstr "Conferma l'indirizzo E-mail"

#: django_sso_app/backend/templates/account/email_confirm.html:16
#, python-format
msgid "Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail address for user %(user_display)s."
msgstr "Per favore conferma che <a href=\"mailto:%(email)s\">%(email)s</a> è un indirizzo e-mail per l'utente %(user_display)s."

#: django_sso_app/backend/templates/account/email_confirm.html:20
msgid "Confirm"
msgstr "Conferma"

#: django_sso_app/backend/templates/account/email_confirm.html:27
#, python-format
msgid "This e-mail confirmation link expired or is invalid. Please <a href=\"%(email_url)s\">issue a new e-mail confirmation request</a>."
msgstr "Questo link di conferma e-mail è scaduto o non è più valido. Per favore <a href=\"%(email_url)s\">richiedine uno nuovo</a>."

#: django_sso_app/backend/templates/account/login.html:7
#: django_sso_app/backend/templates/account/login.html:11
#: django_sso_app/backend/templates/account/login.html:44
#: django_sso_app/backend/templates/partials/nav_footer.html:31
#: django_sso_app/backend/templates/partials/nav_pills.html:41
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:39
msgid "Sign In"
msgstr "Accedi"

#: django_sso_app/backend/templates/account/login.html:16
#, python-format
msgid ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign up</a>\n"
"for a %(site_name)s account and sign in below:"
msgstr ""
"Per favore accedi con uno\n"
"dei tuoi account oppure <a href=\"%(signup_url)s\">registrati</a>\n"
"ad un account %(site_name)s ed accedi:"

#: django_sso_app/backend/templates/account/login.html:31
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."
msgstr ""
"Se non hai ancora creato un account, \n"
"<a href=\"%(signup_url)s\">registrati</a>."

#: django_sso_app/backend/templates/account/login.html:43
msgid "Forgot Password?"
msgstr "Password dimenticata?"

#: django_sso_app/backend/templates/account/logout.html:5
#: django_sso_app/backend/templates/account/logout.html:8
#: django_sso_app/backend/templates/account/logout.html:17
#: django_sso_app/backend/templates/partials/nav_footer.html:23
#: django_sso_app/backend/templates/partials/nav_pills.html:32
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:35
msgid "Sign Out"
msgstr "Esci"

#: django_sso_app/backend/templates/account/logout.html:10
msgid "Are you sure you want to sign out?"
msgstr "Sei sicuro di voler uscire?"

#: django_sso_app/backend/templates/account/password_change.html:6
#: django_sso_app/backend/templates/account/password_change.html:9
#: django_sso_app/backend/templates/account/password_change.html:14
#: django_sso_app/backend/templates/account/password_reset_from_key.html:5
#: django_sso_app/backend/templates/account/password_reset_from_key.html:8
#: django_sso_app/backend/templates/account/password_reset_from_key_done.html:4
#: django_sso_app/backend/templates/account/password_reset_from_key_done.html:7
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:31
msgid "Change Password"
msgstr "Cambia password"

#: django_sso_app/backend/templates/account/password_reset.html:7
#: django_sso_app/backend/templates/account/password_reset.html:11
#: django_sso_app/backend/templates/account/password_reset_done.html:6
#: django_sso_app/backend/templates/account/password_reset_done.html:9
msgid "Password Reset"
msgstr "Reimposta password"

#: django_sso_app/backend/templates/account/password_reset.html:16
msgid "Forgotten your password? Enter your e-mail address below, and we'll send you an e-mail allowing you to reset it."
msgstr "Hai dimenticato la password? Inserisci in tuo indirizzo e-mail, ti invieremo una e-mail per la reimpostazione."

#: django_sso_app/backend/templates/account/password_reset.html:21
msgid "Reset My Password"
msgstr "Reimposta la mia password"

#: django_sso_app/backend/templates/account/password_reset.html:24
msgid "Please contact us if you have any trouble resetting your password."
msgstr "Se dovessi incontrare problemi reimpostando la password ti preghiamo di contattaci."

#: django_sso_app/backend/templates/account/password_reset_done.html:15
#: django_sso_app/core/api/views.py:351
msgid "We have sent you an e-mail. Please contact us if you do not receive it within a few minutes."
msgstr "Ti abbiamo spedito una e-mail. Ti preghiamo di contattarci se non dovessi riceverla in pochi minuti."

#: django_sso_app/backend/templates/account/password_reset_from_key.html:8
msgid "Bad Token"
msgstr ""

#: django_sso_app/backend/templates/account/password_reset_from_key.html:12
#, python-format
msgid "The password reset link was invalid, possibly because it has already been used.  Please request a <a href=\"%(passwd_reset_url)s\">new password reset</a>."
msgstr "Il link per l'impostazione della password non è valido, forse è già stato usato.  Per favore richiedi un <a href=\"%(passwd_reset_url)s\">nuovo link</a>."

#: django_sso_app/backend/templates/account/password_reset_from_key.html:18
msgid "change password"
msgstr "cambio password"

#: django_sso_app/backend/templates/account/password_reset_from_key.html:21
#: django_sso_app/backend/templates/account/password_reset_from_key_done.html:8
#: django_sso_app/core/api/views.py:405
msgid "Your password is now changed."
msgstr "La tua password è stata cambiata."

#: django_sso_app/backend/templates/account/password_set.html:6
#: django_sso_app/backend/templates/account/password_set.html:9
#: django_sso_app/backend/templates/account/password_set.html:14
#: django_sso_app_qcumber_frontend/templates/account/password_set.html:5
msgid "Set Password"
msgstr "Imposta password"

#: django_sso_app/backend/templates/account/profile.html:27
#: django_sso_app/backend/templates/account/profile_complete.html:28
#: django_sso_app/backend/templates/account/profile_update.html:28
msgid "Update"
msgstr "Aggiorna"

#: django_sso_app/backend/templates/account/profile.html:32
#: django_sso_app/backend/templates/account/profile_complete.html:34
#: django_sso_app/backend/templates/account/profile_update.html:34
msgid "You are not logged in."
msgstr "Non hai acceduto."

#: django_sso_app/backend/templates/account/profile.html:38
#: django_sso_app/backend/templates/account/profile_complete.html:40
#: django_sso_app/backend/templates/account/profile_update.html:40
msgid "Login"
msgstr "Accesso"

#: django_sso_app/backend/templates/account/profile.html:42
#: django_sso_app/backend/templates/account/profile_complete.html:46
#: django_sso_app/backend/templates/account/profile_update.html:46
msgid "Register"
msgstr "Registrazione"

#: django_sso_app/backend/templates/account/signup.html:6
msgid "Signup"
msgstr "Registrazione"

#: django_sso_app/backend/templates/account/signup.html:13
#: django_sso_app/backend/templates/account/signup.html:24
#: django_sso_app/backend/templates/partials/nav_footer.html:28
#: django_sso_app/backend/templates/partials/nav_pills.html:37
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:37
msgid "Sign Up"
msgstr "Registrati"

#: django_sso_app/backend/templates/account/signup.html:15
#, python-format
msgid "Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr "Hai già un account? <a href=\"%(login_url)s\">Accedi</a>."

#: django_sso_app/backend/templates/account/signup_closed.html:5
#: django_sso_app/backend/templates/account/signup_closed.html:8
#: django_sso_app_qcumber_frontend/templates/account/signup_closed.html:5
msgid "Sign Up Closed"
msgstr "Accesso disabilitato"

#: django_sso_app/backend/templates/account/signup_closed.html:10
msgid "We are sorry, but the sign up is currently closed."
msgstr "Siamo spiacenti, l'accesso al momento è disabilitato."

#: django_sso_app/backend/templates/account/verification_sent.html:5
#: django_sso_app/backend/templates/account/verification_sent.html:8
#: django_sso_app/backend/templates/account/verified_email_required.html:5
#: django_sso_app/backend/templates/account/verified_email_required.html:8
#: django_sso_app_qcumber_frontend/templates/account/verification_sent.html:5
#: django_sso_app_qcumber_frontend/templates/account/verified_email_required.html:5
msgid "Verify Your E-mail Address"
msgstr "Verifica il tuo indirizzo E-mail"

#: django_sso_app/backend/templates/account/verification_sent.html:10
#: django_sso_app/core/adapter.py:29 django_sso_app/core/api/views.py:271
#: django_sso_app/core/apps/users/tests/test_views.py:428
msgid "We have sent an e-mail to you for verification. Follow the link provided to finalize the signup process. Please contact us if you do not receive it within a few minutes."
msgstr "Ti abbiamo spedito una e-mail di verifica. Segui il link ricevuto per completare il processo di registrazione. Contattaci se non lo ricevi in pochi minuti."

#: django_sso_app/backend/templates/account/verified_email_required.html:12
msgid ""
"This part of the site requires us to verify that\n"
"you are who you claim to be. For this purpose, we require that you\n"
"verify ownership of your e-mail address. "
msgstr ""
"Questa parte del sito è protetta e necessita una verifica \n"
"delle tue credenziali. E' necessario verificare \n"
"l'effettiva proprietà del tuo indirizzo e-mail. "

#: django_sso_app/backend/templates/account/verified_email_required.html:16
msgid ""
"We have sent an e-mail to you for\n"
"verification. Please click on the link inside this e-mail. Please\n"
"contact us if you do not receive it within a few minutes."
msgstr ""
"Ti abbiamo spedito una e-mail di\n"
"verifica. Per favore clicca sul link qui contenuto. \n"
"Contattaci se non lo ricevi in pochi minuti."

#: django_sso_app/backend/templates/account/verified_email_required.html:20
#, python-format
msgid "<strong>Note:</strong> you can still <a href=\"%(email_url)s\">change your e-mail address</a>."
msgstr "<strong>Nota:</strong> puoi sempre <a href=\"%(email_url)s\">cambiare il tuo indirizzo e-mail</a>."

#: django_sso_app/backend/templates/cookie_consent.html:20
#: django_sso_app_qcumber_frontend/templates/cookie_consent.html:20
msgid "This website uses cookies to ensure you get the best experience on our website."
msgstr "Questo sito usa i cookies per assicurarti una navigazione ottimale."

#: django_sso_app/backend/templates/cookie_consent.html:21
#: django_sso_app_qcumber_frontend/templates/cookie_consent.html:21
msgid "GOT IT!"
msgstr "HO CAPITO!"

#: django_sso_app/backend/templates/cookie_consent.html:22
#: django_sso_app_qcumber_frontend/templates/cookie_consent.html:22
msgid "Learn more"
msgstr "Maggiori informazioni"

#: django_sso_app/backend/templates/partials/nav_footer.html:14
#: django_sso_app/backend/templates/partials/nav_pills.html:20
#: django_sso_app/core/apps/profiles/models.py:107
#: django_sso_app_qcumber_frontend/templates/account/profile_update.html:5
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:28
msgid "Profile"
msgstr "Profilo"

#: django_sso_app/backend/templates/partials/nav_footer.html:17
#: django_sso_app/backend/templates/partials/nav_pills.html:24
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:29
msgid "Update Profile"
msgstr "Aggiorna profilo"

#: django_sso_app/backend/templates/partials/nav_footer.html:20
#: django_sso_app/backend/templates/partials/nav_pills.html:28
#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:30
msgid "Change E-mail"
msgstr "Cambia E-mail"

#: django_sso_app/backend/users/apps.py:7
msgid "Users"
msgstr "Utenti"

#: django_sso_app/backend/users/models.py:15
#: django_sso_app/core/apps/profiles/models.py:111
msgid "user"
msgstr ""

#: django_sso_app/backend/users/models.py:16
msgid "users"
msgstr ""

#: django_sso_app/backend/users/views.py:34
msgid "Infos successfully updated"
msgstr ""

#: django_sso_app/config/settings/languages.py:5
msgid "it"
msgstr ""

#: django_sso_app/config/settings/languages.py:6
msgid "en"
msgstr ""

#: django_sso_app/config/settings/languages.py:7
msgid "es"
msgstr ""

#: django_sso_app/config/settings/languages.py:8
msgid "pt"
msgstr ""

#: django_sso_app/config/settings/languages.py:9
msgid "fr"
msgstr ""

#: django_sso_app/config/settings/languages.py:10
msgid "de"
msgstr ""

#: django_sso_app/config/settings/languages.py:11
msgid "nl"
msgstr ""

#: django_sso_app/core/api/serializers.py:101
#, python-format
msgid "E-mail address not verified: %(value)s"
msgstr "Indirizzo E-mail non verificato: %(value)s"

#: django_sso_app/core/api/views.py:321
msgid "We have sent an e-mail to you for verification. Please click on the link inside this e-mail. Please contact us if you do not receive it within a few minutes."
msgstr "Ti abbiamo spedito una e-mail di verifica. Per favore clicca sul link qui contenuto. Contattaci se non lo ricevi in pochi minuti."

#: django_sso_app/core/api/views.py:377
msgid "The password reset link was invalid, possibly because it has already been used.  Please request a <a href=\"{}\">new password reset</a>."
msgstr "Il link per l'impostazione della password non è valido, forse è già stato usato.  Per favore richiedi un <a href=\"%(passwd_reset_url)s\">nuovo link</a>."

#: django_sso_app/core/api/views.py:437
msgid "Password updated, <a href='/login/'>LOGIN</a> with new credentials."
msgstr "Password aggiornata, <a href='/login/'>ACCEDI</a> con le nuove credenziali."

#: django_sso_app/core/api/views.py:463
msgid "Password already set."
msgstr "La password è già stata impostata."

#: django_sso_app/core/api/views.py:472
msgid "Password successfully set."
msgstr "Password impostata con successo."

#: django_sso_app/core/apps/devices/models.py:17
msgid "Device"
msgstr ""

#: django_sso_app/core/apps/devices/models.py:23
#: django_sso_app/core/apps/services/models.py:117
msgid "profile"
msgstr ""

#: django_sso_app/core/apps/devices/models.py:26
msgid "fingerprint"
msgstr ""

#: django_sso_app/core/apps/devices/models.py:27
msgid "user agent"
msgstr ""

#: django_sso_app/core/apps/devices/models.py:29
msgid "apigw_jwt_id"
msgstr ""

#: django_sso_app/core/apps/devices/models.py:30
msgid "apigw_jwt_key"
msgstr ""

#: django_sso_app/core/apps/devices/models.py:31
msgid "apigw_jwt_secret"
msgstr ""

#: django_sso_app/core/apps/groups/models.py:34
msgid "Grouping"
msgstr ""

#: django_sso_app/core/apps/groups/models.py:36
#: django_sso_app/core/apps/services/models.py:24
msgid "name"
msgstr "nome"

#: django_sso_app/core/apps/groups/models.py:38
msgid "type"
msgstr ""

#: django_sso_app/core/apps/groups/models.py:39
#: django_sso_app/core/apps/profiles/models.py:65
msgid "description"
msgstr "descrizione"

#: django_sso_app/core/apps/passepartout/models.py:34
msgid "Passepartout"
msgstr ""

#: django_sso_app/core/apps/passepartout/models.py:40
msgid "token"
msgstr ""

#: django_sso_app/core/apps/passepartout/models.py:41
msgid "jwt"
msgstr ""

#: django_sso_app/core/apps/passepartout/models.py:42
msgid "device"
msgstr ""

#: django_sso_app/core/apps/passepartout/models.py:43
msgid "bumps"
msgstr ""

#: django_sso_app/core/apps/profiles/forms.py:26
#: django_sso_app/core/apps/profiles/forms.py:29
msgid "Social Security Number"
msgstr "Codice Fiscale"

#: django_sso_app/core/apps/profiles/forms.py:32
#: django_sso_app/core/apps/profiles/forms.py:35
msgid "Phone"
msgstr "Telefono"

#: django_sso_app/core/apps/profiles/forms.py:38
#: django_sso_app/core/apps/profiles/forms.py:41
msgid "First name"
msgstr "Nome"

#: django_sso_app/core/apps/profiles/forms.py:44
#: django_sso_app/core/apps/profiles/forms.py:47
msgid "Last name"
msgstr "Cognome"

#: django_sso_app/core/apps/profiles/forms.py:50
#: django_sso_app/core/apps/profiles/forms.py:52
msgid "Description"
msgstr "Descrizione"

#: django_sso_app/core/apps/profiles/forms.py:56
msgid "Picture"
msgstr "Immagine"

#: django_sso_app/core/apps/profiles/forms.py:61
#: django_sso_app/core/apps/profiles/forms.py:64
msgid "Birthdate"
msgstr "Data di nascita"

#: django_sso_app/core/apps/profiles/forms.py:72
msgid "Country of birth"
msgstr "Paese"

#: django_sso_app/core/apps/profiles/forms.py:74
msgid "Latitude"
msgstr "Latitudine"

#: django_sso_app/core/apps/profiles/forms.py:76
msgid "Longitude"
msgstr "Longitudine"

#: django_sso_app/core/apps/profiles/forms.py:85
msgid "Country"
msgstr "Paese"

#: django_sso_app/core/apps/profiles/forms.py:88
#: django_sso_app/core/apps/profiles/forms.py:90
msgid "Address"
msgstr "Indirizzo"

#: django_sso_app/core/apps/profiles/forms.py:94
#: django_sso_app/core/apps/profiles/forms.py:97
msgid "Language"
msgstr "Lingua"

#: django_sso_app/core/apps/profiles/forms.py:100
#: django_sso_app/core/apps/profiles/forms.py:103
msgid "Gender"
msgstr ""

#: django_sso_app/core/apps/profiles/forms.py:106
#: django_sso_app/core/apps/profiles/forms.py:109
msgid "Company name"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:45
msgid "sso id"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:46
msgid "sso revision"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:48
msgid "user username"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:49
#: django_sso_app/core/apps/users/models.py:16
msgid "email address"
msgstr "indirizzo E-mail"

#: django_sso_app/core/apps/profiles/models.py:51
msgid "external id"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:53
msgid "social security number"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:55
msgid "Phone number must be entered in the format: '+999999999'. Up to 16 digits allowed."
msgstr "Il numero di telefono deve seguire il formato: +399999999999. Sono permesse al massimo 16 cifre."

#: django_sso_app/core/apps/profiles/models.py:59
msgid "first name"
msgstr "nome"

#: django_sso_app/core/apps/profiles/models.py:61
msgid "last name"
msgstr "cognome"

#: django_sso_app/core/apps/profiles/models.py:63
msgid "alias"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:66
#: django_sso_app/core/apps/services/models.py:26
msgid "picture"
msgstr "immagine"

#: django_sso_app/core/apps/profiles/models.py:68
msgid "birthdate"
msgstr "data di nascita"

#: django_sso_app/core/apps/profiles/models.py:69
msgid "country of birth"
msgstr "paese"

#: django_sso_app/core/apps/profiles/models.py:71
msgid "latitude"
msgstr "latitudine"

#: django_sso_app/core/apps/profiles/models.py:72
msgid "longitude"
msgstr "longitudine"

#: django_sso_app/core/apps/profiles/models.py:76
msgid "country"
msgstr "paese"

#: django_sso_app/core/apps/profiles/models.py:78
msgid "address"
msgstr "indirizzo"

#: django_sso_app/core/apps/profiles/models.py:79
#: django_sso_app/core/apps/services/models.py:94
msgid "language"
msgstr "lingua"

#: django_sso_app/core/apps/profiles/models.py:81
msgid "gender"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:83
msgid "company name"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:85
msgid "expiration date"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:87
msgid "profile completion date"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:89
msgid "unsubscription date"
msgstr "data di de-registrazione"

#: django_sso_app/core/apps/profiles/models.py:91
msgid "meta"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:93
msgid "created by event"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:114
msgid "API gateway consumer id"
msgstr ""

#: django_sso_app/core/apps/profiles/models.py:116
#: django_sso_app/core/apps/services/models.py:34
msgid "groups"
msgstr ""

#: django_sso_app/core/apps/services/models.py:22
msgid "Service"
msgstr ""

#: django_sso_app/core/apps/services/models.py:25
msgid "service url"
msgstr ""

#: django_sso_app/core/apps/services/models.py:28
msgid "cookie domain"
msgstr ""

#: django_sso_app/core/apps/services/models.py:29
msgid "redirect wait"
msgstr ""

#: django_sso_app/core/apps/services/models.py:31
msgid "subscription required"
msgstr ""

#: django_sso_app/core/apps/services/models.py:32
#: django_sso_app/core/models.py:52
msgid "is public"
msgstr ""

#: django_sso_app/core/apps/services/models.py:92
msgid "TOS"
msgstr ""

#: django_sso_app/core/apps/services/models.py:95
msgid "text"
msgstr ""

#: django_sso_app/core/apps/services/models.py:98
#: django_sso_app/core/apps/services/models.py:123
msgid "service"
msgstr ""

#: django_sso_app/core/apps/services/models.py:110
msgid "Subscription"
msgstr "Iscrizione"

#: django_sso_app/core/apps/services/models.py:125
msgid "unsubscribed at"
msgstr ""

#: django_sso_app/core/apps/status/models.py:18
msgid "Status"
msgstr ""

#: django_sso_app/core/apps/users/admin.py:31
msgid "Permissions"
msgstr ""

#: django_sso_app/core/apps/users/forms.py:16
msgid "This username has already been taken."
msgstr "Questo username è già utilizzato."

#: django_sso_app/core/apps/users/views.py:171
msgid "Email already registered."
msgstr "Email già registrata."

#: django_sso_app/core/models.py:21
msgid "is active"
msgstr ""

#: django_sso_app/core/models.py:28
msgid "created at"
msgstr ""

#: django_sso_app/core/models.py:37
msgid "updated at"
msgstr ""

#: django_sso_app/core/models.py:59
msgid "started at"
msgstr ""

#: django_sso_app/core/models.py:61
msgid "ended at"
msgstr ""

#: django_sso_app/core/models.py:68
msgid "Meta"
msgstr ""

#: django_sso_app/core/to_translate.py:4
msgid "Please Confirm Your E-mail Address"
msgstr "Conferma il tuo indirizzo E-mail"

#: django_sso_app/core/to_translate.py:5
msgid "E-mail is not verified."
msgstr "E-mail non verificata."

#: django_sso_app/core/tokens/authentication.py:67
msgid "Authorization header must contain two space-delimited values"
msgstr ""

#: django_sso_app/core/tokens/authentication.py:90
msgid "Given token not valid for any token type"
msgstr ""

#: django_sso_app/core/tokens/authentication.py:101
msgid "Token contained no recognizable user identification"
msgstr ""

#: django_sso_app/core/tokens/authentication.py:106
msgid "User not found"
msgstr "Utente non trovato"

#: django_sso_app/core/tokens/authentication.py:109
msgid "User is inactive"
msgstr "Questo utente è disattivato"

#: django_sso_app_qcumber_frontend/templates/account/base.html:115
#: django_sso_app_qcumber_frontend/templates/account/email.html:115
#: django_sso_app_qcumber_frontend/templates/account/login.html:118
#: django_sso_app_qcumber_frontend/templates/account/logout.html:115
#: django_sso_app_qcumber_frontend/templates/account/password_change.html:115
#: django_sso_app_qcumber_frontend/templates/account/password_reset.html:115
#: django_sso_app_qcumber_frontend/templates/account/password_set.html:115
#: django_sso_app_qcumber_frontend/templates/account/profile_complete.html:115
#: django_sso_app_qcumber_frontend/templates/account/profile_update.html:115
#: django_sso_app_qcumber_frontend/templates/account/signup.html:115
msgid "Please enable javascript."
msgstr ""

#: django_sso_app_qcumber_frontend/templates/account/base.html:123
#: django_sso_app_qcumber_frontend/templates/account/email.html:123
#: django_sso_app_qcumber_frontend/templates/account/login.html:126
#: django_sso_app_qcumber_frontend/templates/account/logout.html:123
#: django_sso_app_qcumber_frontend/templates/account/password_change.html:123
#: django_sso_app_qcumber_frontend/templates/account/password_reset.html:123
#: django_sso_app_qcumber_frontend/templates/account/password_set.html:123
#: django_sso_app_qcumber_frontend/templates/account/profile_complete.html:123
#: django_sso_app_qcumber_frontend/templates/account/profile_update.html:123
#: django_sso_app_qcumber_frontend/templates/account/signup.html:123
msgid "Loading.."
msgstr "Caricamento.."

#: django_sso_app_qcumber_frontend/templates/partials/footer_copyright.html:33
msgid "Home"
msgstr ""
